// StemRankerPlus.java
// JG Miller (JGM), Portland, OR, jg.millirem@gmail.com
// 3/23/15 -> 4/12/15, 6/22/15
// 2/17/19 - Altered original StemRanker program to produce custom list for Sam Kantimathi, of top X Mike Baron racks which don't themselves have an anagram.
//
// Uses algorithm published in Mike Baron's Scrabble Wordbook, Sterling Publishing Co. Inc., 2004:
// https://books.google.com/books?id=NQ6gD_S9unYC&lpg=PA5&ots=RDuiZJxvsu&dq=top
// %20100%20seven%20letter%20bingo%20stems%20based%20on%20mmpr&pg=PA37#v=onepage
// &q=top%20100%20seven%20letter%20bingo%20stems%20based%20on%20mmpr&f=false

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import static java.lang.Double.valueOf;

public class StemRankerPlus {
    private static final String LEXICON_NAME = "CSW19";
    private static final String WORD_FILE_NAME_PREFIX =
            "F:\\jim\\Documents\\My Documents\\GAMES\\SCRABBLE\\" + LEXICON_NAME + "\\Stem lists generation\\" + LEXICON_NAME + "_";
    private static final String WORD_FILE_NAME_SUFFIX = "s.txt";
    private static final boolean OUTPUT_TO_CONSOLE = false;
    private static final boolean APPEND_OUTPUT_FILE = false;
    private static final String OUTPUT_FILE_NAME_PREFIX =
            "F:\\jim\\Documents\\My Documents\\GAMES\\SCRABBLE\\" + LEXICON_NAME + "\\Stem lists generation\\" + LEXICON_NAME + "_";
    private static final String DETAILS_FILE_NAME_SUFFIX = "-letter-stems_fulldetails.txt";
    private static final String SUMMARY_FILE_NAME_SUFFIX = "-letter-stems_summary.txt";
    private static final String NO_BINGOS_8S_FILE_NAME_SUFFIX = "NO_BINGOS_8s.txt";
    private static final int WORD_LENGTH_MIN = 2;
    private static final int WORD_LENGTH_MAX = 15;
    private static final String RUN_DATE = "June, 2019";
    private static final int PAGE_WIDTH = 102;
    private static final int MID_WIDTH = 62;
    private static final int PAGE_ONE_ROWS = 55;
    private static final int FULL_PAGE_ROWS = 61;
    private static String wordFileName;
    private static int wordLength;
    private static int stemLength;
    private static int minColumnSpacing;
    private static int numInitialPageColumns;
    private static int numColumns;
    private static int numPageItems;
    private static boolean page1;
    private static int maxPageItemLength;
    private static int numInterColumnBlanks;
    private static ArrayList<String> pageStrings;
    private static HashMap<String, StemData> stemMap;
    private static HashMap<String, StemData> stemMap7s;
    private static HashMap<String, StemData> stemMap8s;
    private static ArrayList<StemData> stemMapValuesList;
    private static int SN;
    private static HashSet<Character> lettersHashSet;
    private static double topStemAdjustment;
    private static ArrayList<String> bingoList;
    private static String stemIntroLine;
    private static int stemColumnWidth;
    private static String dataLine;
    private static StringBuilder lettersBuilderUnique;
    private static StringBuilder lettersBuilder;

    public StemRankerPlus() {
        pageStrings = new ArrayList<String>();
        stemMap = new HashMap<String, StemData>();
        stemMapValuesList = new ArrayList<StemData>();
        lettersHashSet = new HashSet<Character>();
    }

    private static void reset() {
        page1 = true;
        maxPageItemLength = 0;
        SN = 0;
        topStemAdjustment = 1.0;
        pageStrings.clear();
        stemMap.clear();
        stemMapValuesList.clear();
    }

    public static void main(String[] args) {
        StemRankerPlus stemRankerPlus = new StemRankerPlus();
        File outputFile;
        FileOutputStream fos;
        PrintStream ps;

        System.out.println("Started:  " + LocalDateTime.now());
        PrintStream console = System.out;
        if (args.length == 0)
            for (wordLength = WORD_LENGTH_MIN; wordLength <= WORD_LENGTH_MAX; wordLength++) {
                stemLength = wordLength - 1;   ///*** Generalize later!
                    stemRankerPlus.execute();
                    reset();
            }
        else {
            wordLength = Integer.parseInt(args[0]);
            stemRankerPlus.execute();
            reset();
        }

        if (!OUTPUT_TO_CONSOLE) {
            try {
                outputFile = new File(OUTPUT_FILE_NAME_PREFIX + NO_BINGOS_8S_FILE_NAME_SUFFIX);
                fos = new FileOutputStream(outputFile, APPEND_OUTPUT_FILE);
                ps = new PrintStream(fos);
                System.setOut(ps);
            } catch (FileNotFoundException fnfex) {}
        }
        processRacksWithNoSevensCsv();
        reset();

        System.setOut(console);
        System.out.println("Finished:  " + LocalDateTime.now());
    }

    private void execute() {
        File outputFile;
        FileOutputStream fos;
        PrintStream ps;

        wordFileName = WORD_FILE_NAME_PREFIX + wordLength + WORD_FILE_NAME_SUFFIX;
        calculateStemMap();
        if (!OUTPUT_TO_CONSOLE) {
            try {
                outputFile = new File(OUTPUT_FILE_NAME_PREFIX + wordLength + "s_" + stemLength + SUMMARY_FILE_NAME_SUFFIX);
                fos = new FileOutputStream(outputFile, APPEND_OUTPUT_FILE);
                ps = new PrintStream(fos);
                System.setOut(ps);
            } catch (FileNotFoundException fnfex) {}
        }
        //outputSummaryStemData();
        outputSummaryStemDataCsv();

        if (!OUTPUT_TO_CONSOLE) {
            try {
                outputFile = new File(OUTPUT_FILE_NAME_PREFIX + wordLength + "s_" + stemLength + DETAILS_FILE_NAME_SUFFIX);
                fos = new FileOutputStream(outputFile, APPEND_OUTPUT_FILE);
                ps = new PrintStream(fos);
                System.setOut(ps);
            } catch (FileNotFoundException fnfex) {
            }
        }
        //outputFullBingoData();
        outputColumnizedFullBingoData();
    }

    private static void processRacksWithNoSevensCsv() {
        ArrayList<StemData> stemMapValuesList8s = new ArrayList<StemData>();
        String curr8Stem;
        String curr7Stem;
        Character curr8Letter;
        Character prev8Letter = '\0';
        boolean curr8StemFound;

        stemMapValuesList8s.addAll(stemMap8s.values());
        Collections.sort(stemMapValuesList8s, StemData::compareBySN);
        System.out.println("SN,STEM,MMPR,MSP,#UT,#UL,#WORDS,MNEMONIC,ANAHOOK LETTERS,ANAHOOK WORDS");
        for (StemData stemData : stemMapValuesList8s) {
            curr8Stem = stemData.stem;
            curr8StemFound = false;
            for (int i = 0; i < curr8Stem.length(); i++) {
                curr8Letter = curr8Stem.charAt(i);
                if (curr8Letter != prev8Letter) {
                    prev8Letter = curr8Letter;
                    curr7Stem = curr8Stem.substring(0, i).concat(curr8Stem.substring(i + 1, curr8Stem.length()));
                    if (stemMap7s.containsKey(curr7Stem)) {
                        if (stemMap7s.get(curr7Stem).validLettersUnique.indexOf(curr8Letter) != -1) {
                            curr8StemFound = true;
                            break;
                        }
                    }
                }
            }
            if (!curr8StemFound)
                System.out.println(stemData.toString());
        }
    }

    private void calculateStemMap() {
        String word, sortedWord, currStem;
        char[] wordChars;
        char currLetter, prevLetter;
        int currLetterTileCount;
        char currStemLetter = '\0', prevStemLetter = '\0';
        int prevStemLetterCount;
        int usedCurrStemCurrLetterCount;
        int currStemCurrLetterAvailCount;
        int currStemAvailBlanks;
        int currStemLetterOverage;
        StemData currData;
        ArrayList<String> currBingoList;
        HashMap<Character, Integer> blankUsingStemTiles = new HashMap<Character, Integer>();

        // Analyze bingo list (of equal-length words) into stem map data structure.
        try {
            BufferedReader br = new BufferedReader(new FileReader(wordFileName));
            for (; (word = br.readLine()) != null; ) {
                prevLetter = '\0';
                word = word.toUpperCase();
                // Sort word string into alphabetical order (stem form).
                wordChars = word.toCharArray();
                Arrays.sort(wordChars);
                sortedWord = new String(wordChars);
                for (int i = 0; i < sortedWord.length(); i++) {
                    currLetter = sortedWord.charAt(i);
                    if (currLetter != prevLetter) {
                        prevLetter = currLetter;
                        currStem = sortedWord.substring(0, i).concat(sortedWord.substring(i + 1, sortedWord.length()));
                        usedCurrStemCurrLetterCount = 0;
                        if (!stemMap.containsKey(currStem)) {
                            prevStemLetterCount = 0;
                            blankUsingStemTiles.clear();
                            currStemAvailBlanks = 2;
                            for (int j = 0; j <= currStem.length(); j++) {
                                if (j < currStem.length())
                                    currStemLetter = currStem.charAt(j);
                                if (j == 0) {
                                    prevStemLetter = currStemLetter;
                                    prevStemLetterCount++;
                                }
                                else if (currStemLetter != prevStemLetter || j == currStem.length()) {
                                    currLetterTileCount = RealTileCount.valueOf(String.valueOf(prevStemLetter)).count();
                                    currStemLetterOverage = prevStemLetterCount - currLetterTileCount;
                                    if (currStemLetterOverage > 0) {
                                        currStemAvailBlanks -= currStemLetterOverage;
                                        blankUsingStemTiles.put(prevStemLetter, (currStemLetterOverage <= 2 ? currStemLetterOverage : 2));
                                    }
                                    prevStemLetterCount = 1;
                                    prevStemLetter = currStemLetter;
                                }
                                else
                                    prevStemLetterCount++;
                            }
                            if (currStemAvailBlanks < 0)
                                stemMap.put(currStem, new StemData(currStemAvailBlanks, 0, currStem, 0, '\0', word, currStemAvailBlanks));
                            else {
                                currLetterTileCount = RealTileCount.valueOf(String.valueOf(currLetter)).count();
                                usedCurrStemCurrLetterCount = currStem.length() - currStem.replace(String.valueOf(currLetter), "").length();
                                currStemCurrLetterAvailCount = currLetterTileCount - usedCurrStemCurrLetterCount + currStemAvailBlanks
                                        + blankUsingStemTiles.getOrDefault(currLetter, 0);
                                if (currStemCurrLetterAvailCount <= 0)
                                    stemMap.put(currStem, new StemData(0, 0, currStem, 0, '\0', word, currStemAvailBlanks));
                                else
                                    stemMap.put(currStem, new StemData(currStemCurrLetterAvailCount, 1, currStem, 1, currLetter, word, currStemAvailBlanks));
                            }
                        }
                        else if (stemMap.get(currStem).UT < 0)
                            continue;
                        else {
                            currLetterTileCount = RealTileCount.valueOf(String.valueOf(currLetter)).count();
                            currStemAvailBlanks = stemMap.get(currStem).availBlanks;
                            usedCurrStemCurrLetterCount = currStem.length() - currStem.replace(String.valueOf(currLetter), "").length();
                            currStemCurrLetterAvailCount = currLetterTileCount - usedCurrStemCurrLetterCount + currStemAvailBlanks
                                    + blankUsingStemTiles.getOrDefault(currLetter, 0);
                            currData = stemMap.get(currStem);
                            if (currStemCurrLetterAvailCount > 0) {
                                currData.numBingos++;
                                currData.validLetters.add(currLetter);
                                if (currData.bingoMap.containsKey(currLetter)) {
                                    currBingoList = currData.bingoMap.get(currLetter);
                                    currBingoList.add(word);
                                    currData.bingoMap.replace(currLetter, currBingoList);
                                }
                                else {
                                    currData.UL++;
                                    currData.UT += (currStemCurrLetterAvailCount - currStemAvailBlanks);
                                    currBingoList = new ArrayList<String>();
                                    currBingoList.add(word);
                                    currData.bingoMap.put(currLetter, currBingoList);
                                }
                            }
                            else
                            if (currData.bingoMap.containsKey('\0')) {
                                currBingoList = currData.bingoMap.get('\0');
                                currBingoList.add(word);
                                currData.bingoMap.replace('\0', currBingoList);
                            }
                            else {
                                currBingoList = new ArrayList<String>();
                                currBingoList.add(word);
                                currData.bingoMap.put('\0', currBingoList);
                            }
                            stemMap.replace(currStem, currData);
                        }
                    }
                }
            }
            br.close();
        } catch (IOException ioex) { }

        // Make final computations and orderings on stem map data structure.
        stemMap.forEach((k, v) -> v.MSP = calculateMSP(k));
        stemMapValuesList.addAll(stemMap.values());
        stemMapValuesList.forEach((data) -> {
            data.MMPR = data.MSP * data.UT;
        });
        Collections.sort(stemMapValuesList);
        int i = 0;
        // I'm not 100% sure that this is the correct idea for the 1.5 multiplier for a top 'S' stem....
        // Assuming 'S' appears in *some* stem.
        while (i < stemMapValuesList.size()) {
            if (stemMapValuesList.get(i).stem.indexOf('S') != -1) {
                topStemAdjustment = stemMapValuesList.get(i).MSP / 1.5;
                break;
            }
            i++;
        }
        stemMapValuesList.forEach((data) -> {
            data.MSP = data.MSP / topStemAdjustment;
            data.MMPR = data.MSP * data.UT;
        });
        stemMapValuesList.forEach((data) -> {
            data.SN = ++SN;
            Collections.sort(data.validLetters);
            lettersHashSet.clear();
            lettersHashSet.addAll(data.validLetters);
            data.validLettersUnique.addAll(lettersHashSet);
            Collections.sort(data.validLettersUnique);
            data.bingoMap.forEach((letter, bingoData) -> {
                Collections.sort(bingoData);
            });
        });

        if (wordLength == 7)
            stemMap7s = (HashMap<String, StemData>) stemMap.clone();
        else if (wordLength == 8)
            stemMap8s = (HashMap<String, StemData>) stemMap.clone();
    }

    private double calculateMSP(String stem) {
        double MSP = 1;
        char currLetter;
        char prevLetter = '\0';
        int prevLetterCount = 0;
        int currTileCount;
        int probNumerator;

        for (int i = 0; i < stem.length(); i++) {
            currLetter = stem.charAt(i);
            if (i == 0) {
                prevLetter = currLetter;
                prevLetterCount++;
            }
            else if (currLetter != prevLetter) {
                currTileCount = RealTileCount.valueOf(String.valueOf(prevLetter)).count();
                probNumerator = 1;
                for (int j = 0; j < prevLetterCount; j++) {
                    probNumerator = probNumerator * currTileCount;
                    currTileCount--;
                }
                MSP *= (probNumerator / factorial(prevLetterCount));
                prevLetterCount = 1;
                prevLetter = currLetter;
            }
            else
                prevLetterCount++;
        }
        currTileCount = RealTileCount.valueOf(String.valueOf(prevLetter)).count();
        probNumerator = 1;
        for (int j = 0; j < prevLetterCount; j++) {
            probNumerator = probNumerator * currTileCount;
            currTileCount--;
        }
        MSP *= (probNumerator / factorial(prevLetterCount));
        if (stem.indexOf('S') != -1)
            MSP *= 1.5;
        return MSP;
    }

    private int factorial(int n) {
        if (n == 0) return 1;
        return n * factorial(n - 1);
    }

    //TODO  Fix to correctly calculate multi-lines/indents with unique stem letters column added now!
    private void outputSummaryStemData() {

        System.out.format("*** %s, " + stemLength + "-letter stems, full summary ***%n%n", FilenameUtils.getBaseName(wordFileName));
        System.out.print("Uses algorithm published in Mike Baron's The Complete Wordbook for Game Players, Sterling Publishing Co. Inc., 2004:  ");
        System.out.print("https://books.google.com/books?id=NQ6gD_S9unYC&lpg=PA5&ots=RDuiZJxvsu&dq=top" +
                "%20100%20seven%20letter%20bingo%20stems%20based%20on%20mmpr&pg=PA37#v=onepage" +
                "&q=top%20100%20seven%20letter%20bingo%20stems%20based%20on%20mmpr&f=false    ");
        System.out.print("     (Jim Miller, jg.millirem@gmail.com, PDX, " + RUN_DATE + ".  Distribute freely.)\n\n\n");
        stemColumnWidth = (stemLength >= 4 ? stemLength : 4);
        System.out.format("  SN      %sSTEM       %sMMPR         MSP      UT     UL    #WORDS"
                        + "   MNEMONIC   ALL LETTERS%n", StringUtils.repeat(" ", ((int)(Math.ceil((double)(stemLength - 4) / 2)))),
                StringUtils.repeat(" ", (stemLength - 4) / 2));
        System.out.format("======    %s     ========     ======     ==     ==    ======   ========   ===========%n",
                StringUtils.repeat("=", stemColumnWidth));
        stemMapValuesList.forEach((data) -> {
            int substringIndexStart;

            lettersBuilderUnique = new StringBuilder(data.validLettersUnique.size());
            for (Character ch : data.validLettersUnique)
                lettersBuilderUnique.append(ch);
            lettersBuilder = new StringBuilder(data.validLetters.size());
            for (Character ch : data.validLetters)
                lettersBuilder.append(ch);
            dataLine = String.format("%6d    %" + stemColumnWidth + "s    %9.4f    %7.4f     %2d     %2d      %3d    %s   %s", data.SN, data.stem, data.MMPR, data.MSP, data.UT, data.UL, data.numBingos, lettersBuilderUnique.toString(), lettersBuilder.toString());
            substringIndexStart = 0;
            System.out.print(dataLine.substring(substringIndexStart, (substringIndexStart + PAGE_WIDTH >= dataLine.length()
                    ? dataLine.length() : substringIndexStart + PAGE_WIDTH)));
            substringIndexStart += PAGE_WIDTH;
            while (dataLine.length() > substringIndexStart) {
                System.out.format("%n%s%s", StringUtils.repeat(" ", MID_WIDTH + stemColumnWidth), dataLine.substring(substringIndexStart,
                        (substringIndexStart + (PAGE_WIDTH - (MID_WIDTH + stemColumnWidth)) >= dataLine.length() ? dataLine.length()
                                : substringIndexStart + (PAGE_WIDTH - (MID_WIDTH + stemColumnWidth)))));
                substringIndexStart += (PAGE_WIDTH - (MID_WIDTH + stemColumnWidth));
            }
            System.out.format("%n");
        });
    }

    private static void outputSummaryStemDataCsv() {

        System.out.println("SN,STEM,MMPR,MSP,#UT,#UL,#WORDS,MNEMONIC,ANAHOOK LETTERS,ANAHOOK WORDS");
        stemMapValuesList.forEach((data) -> {
            System.out.println(data.toString());
        });
    }

    private void outputFullBingoData() {

        System.out.format("*** %s, " + stemLength + "-letter stems, full details ***%n%n", FilenameUtils.getBaseName(wordFileName));
        System.out.print("Uses algorithm published in Mike Baron's The Complete Wordbook for Game Players,"
                + " Sterling Publishing Co. Inc., 2004:  ");
        System.out.print("https://books.google.com/books?id=NQ6gD_S9unYC&lpg=PA5&ots=RDuiZJxvsu&dq=top" +
                "%20100%20seven%20letter%20bingo%20stems%20based%20on%20mmpr&pg=PA37#v=onepage" +
                "&q=top%20100%20seven%20letter%20bingo%20stems%20based%20on%20mmpr&f=false    ");
        System.out.print("     (Jim Miller, jg.millirem@gmail.com, PDX, " + RUN_DATE + ".  Distribute freely.)\n\n\n");
        stemMapValuesList.forEach((data) -> {
            stemIntroLine = data.SN + "  " + data.stem;
            System.out.format("%s%n", stemIntroLine);
            System.out.format("%s%n", StringUtils.repeat("=", stemIntroLine.length()));
            for (Character ch : data.validLettersUnique) {
                bingoList = data.bingoMap.get(ch);
                System.out.format("%c  %s%n", ch, bingoList.get(0));
                for (int i = 1; i < bingoList.size(); i++)
                    System.out.format("   %s%n", bingoList.get(i));
            }
            System.out.format("%n");
        });
    }

    private void outputColumnizedFullBingoData() {

        determineNumColumns();
        System.out.format("*** %s, " + stemLength + "-letter stems, full details ***%n%n", FilenameUtils.getBaseName(wordFileName));
        System.out.print("Uses algorithm published in Mike Baron's The Complete Wordbook for Game Players,"
                + " Sterling Publishing Co. Inc., 2004:  ");
        System.out.print("https://books.google.com/books?id=NQ6gD_S9unYC&lpg=PA5&ots=RDuiZJxvsu&dq=top" +
                "%20100%20seven%20letter%20bingo%20stems%20based%20on%20mmpr&pg=PA37#v=onepage" +
                "&q=top%20100%20seven%20letter%20bingo%20stems%20based%20on%20mmpr&f=false    ");
        System.out.print("     (Jim Miller, jg.millirem@gmail.com, PDX, " + RUN_DATE + ".  Distribute freely.)\n\n\n");
        stemMapValuesList.forEach((data) -> {
            stemIntroLine = data.SN + "  " + data.stem;
            try {
                ByteArrayOutputStream baos;
                PrintStream ps;
                String item;

                baos = new ByteArrayOutputStream();
                ps = new PrintStream(baos);
                ps.format("%s", stemIntroLine); item = baos.toString(Charset.defaultCharset().name()); baos.reset();
                columnizeOutput(false, item);
                ps.format("%s", StringUtils.repeat("=", stemIntroLine.length()));
                item = baos.toString(Charset.defaultCharset().name()); baos.reset();
                columnizeOutput(false, item);
                for (Character ch : data.validLettersUnique) {
                    bingoList = data.bingoMap.get(ch);
                    ps.format("%c  %s", ch, bingoList.get(0)); item = baos.toString(Charset.defaultCharset().name()); baos.reset();
                    columnizeOutput(false, item);
                    for (int i = 1; i < bingoList.size(); i++) {
                        ps.format("   %s", bingoList.get(i)); item = baos.toString(Charset.defaultCharset().name()); baos.reset();
                        columnizeOutput(false, item);
                    }
                }
                ps.format(""); item = baos.toString(Charset.defaultCharset().name()); baos.reset();
                columnizeOutput(false, item);
                ps.close();
            } catch (IOException ex) { }
        });
        columnizeOutput(true, "");
    }

    private void columnizeOutput(boolean endOfData, String item) {
        int distanceFromColumnEnd;
        int numPageRows = (page1 ? PAGE_ONE_ROWS : FULL_PAGE_ROWS);
        ArrayList<String> tempPageStrings;

        determineNumColumns();
        if (pageStrings.size() < numPageItems && !endOfData) {
            if (item.equals("")) {
                distanceFromColumnEnd = numPageRows - pageStrings.size() % numPageRows;
                if (distanceFromColumnEnd < 4)
                    if (distanceFromColumnEnd > 0)
                        for (int i = 0; i < distanceFromColumnEnd; i++) {
                            pageStrings.add("");
                            if (pageStrings.size() == numPageItems)
                                flushFullPage(endOfData);
                        }
                    else pageStrings.add(item);
                else if (distanceFromColumnEnd != numPageRows)
                    pageStrings.add(item);
            }
            else
                pageStrings.add(item);
            if (item.length() > maxPageItemLength)
                maxPageItemLength = item.length();
        }
        else {
            if (numColumns < numInitialPageColumns) {
                tempPageStrings = new ArrayList<String>();
                for (int i = numPageItems; i < pageStrings.size(); i++)
                    tempPageStrings.add(pageStrings.get(i));
                pageStrings.subList(numPageItems, pageStrings.size()).clear();
                flushFullPage(false);
                pageStrings.clear();
                pageStrings.addAll(tempPageStrings);
                numInitialPageColumns = numColumns;
                columnizeOutput(endOfData, item);
            }
            else {
                flushFullPage(endOfData);
                if (!endOfData)
                    columnizeOutput(endOfData, item);
            }
        }
    }

    private void flushFullPage(boolean endOfData) {
        String item;
        int numUsedPageRows;
        int numExtraItemSpaces;
        int maxPossiblePageRows = (page1 ? PAGE_ONE_ROWS : FULL_PAGE_ROWS);
        int numUsedColumns;

        if (endOfData && pageStrings.size() != numPageItems) {
            numUsedColumns = pageStrings.size() / maxPossiblePageRows + 1;
            numExtraItemSpaces = (maxPossiblePageRows - pageStrings.size() % maxPossiblePageRows) % maxPossiblePageRows;
            numUsedPageRows = (pageStrings.size() >= maxPossiblePageRows ? maxPossiblePageRows : pageStrings.size());
        }
        else {
            numUsedColumns = numColumns;
            numExtraItemSpaces = 0;
            numUsedPageRows = maxPossiblePageRows;
        }
        numInterColumnBlanks = (PAGE_WIDTH - maxPageItemLength * numColumns) / (numColumns - 1);
        for (int i = 0; i < numUsedPageRows; i++) {
            for (int j = 0; j < numUsedColumns - 1; j++) {
                item = pageStrings.get(i + j * numUsedPageRows);
                System.out.print(item + StringUtils.repeat(" ", numInterColumnBlanks + maxPageItemLength - item.length()));
            }
            if (i < maxPossiblePageRows - numExtraItemSpaces)
                System.out.println(pageStrings.get(i + (numUsedColumns - 1) * numUsedPageRows));
            else
            if (i != numUsedPageRows - 1)
                System.out.println();
        }
        pageStrings.clear();
        maxPageItemLength = 0;
        if (page1) page1 = false;
    }

    private void determineNumColumns() {

        minColumnSpacing = (wordLength >= 4 ? 6 : 7);    ///*** CUSTOMIZE THIS!
        numInitialPageColumns = numColumns;
        numColumns = (PAGE_WIDTH - maxPageItemLength) / (maxPageItemLength + minColumnSpacing) + 1;
        numPageItems = (page1 ? PAGE_ONE_ROWS : FULL_PAGE_ROWS) * numColumns;
    }

    private class StemData implements Comparable<StemData>, Cloneable {
        int SN;
        String stem;
        double MSP;
        int UT;
        int UL;
        double MMPR;
        int numBingos;
        int availBlanks;
        ArrayList<Character> validLetters;
        ArrayList<Character> validLettersUnique;
        HashMap<Character, ArrayList<String>> bingoMap;

        private StemData(int UT, int UL, String stem, int numBingos, char validLetter, String word, int availBlanks) {
            ArrayList<String> newBingoList = new ArrayList<String>();

            this.UT = UT;
            this.UL = UL;
            this.stem = stem;
            this.numBingos = numBingos;
            this.availBlanks = availBlanks;
            this.MMPR = 0.0;
            validLetters = new ArrayList<Character>();
            bingoMap = new HashMap<Character, ArrayList<String>>();
            validLettersUnique = new ArrayList<Character>();
            if (validLetter != '\0') {
                validLetters.add(validLetter);
                newBingoList.add(word);
                bingoMap.put(validLetter, newBingoList);
            }
        }

        // Copy constructor
        public StemData(StemData stemData) {
            this.SN = stemData.SN;
            this.UT = stemData.UT;
            this.UL = stemData.UL;
            this.stem = stemData.stem;
            this.numBingos = stemData.numBingos;
            this.availBlanks = stemData.availBlanks;
            this.MSP = stemData.MSP;
            this.MMPR = stemData.MMPR;
            this.validLetters = stemData.validLetters;
            this.validLettersUnique = stemData.validLettersUnique;
            this.bingoMap = stemData.bingoMap;
        }

        @Override
        public int compareTo(StemData data) {
            int MMPRCmp;
            int MSPCmp;

            MMPRCmp = (valueOf(data.MMPR)).compareTo(valueOf(MMPR));
            if (MMPRCmp != 0)
                return MMPRCmp;
            else {
                MSPCmp = (valueOf(data.MSP)).compareTo(valueOf(MSP));
                return (MSPCmp != 0 ? MSPCmp : stem.compareTo(data.stem));
            }
        }

        public int compareBySN(StemData data) {

            return valueOf(SN).compareTo(valueOf(data.SN));
        }

        @Override
        public Object clone() throws CloneNotSupportedException {

            return super.clone();
        }

        @Override
        public String toString() {
            String stemDataString;
            DecimalFormat df4 = new DecimalFormat("0.0000");
            int bingoMapCounter = 0;
            ArrayList<String> currBingoList;

            stemDataString = this.SN + "," + this.stem + "," + df4.format(this.MMPR) + "," + df4.format(this.MSP) + "," + this.UT + "," + this.UL + "," + this.numBingos + "," + getStringRepresentationOfCharArray(this.validLettersUnique) + "," + getStringRepresentationOfCharArray(this.validLetters) + ",";
            for (Character bingoChar : validLettersUnique) {
                currBingoList = this.bingoMap.get(bingoChar);
                bingoMapCounter++;
                stemDataString += getStringRepresentationOfStringArray(currBingoList);
                if (bingoMapCounter != validLettersUnique.size())
                    stemDataString += " ";
            }
            return stemDataString;
        }

        String getStringRepresentationOfCharArray(ArrayList<Character> list) {

            StringBuilder builder = new StringBuilder(list.size());
            for (Character ch: list)
                builder.append(ch);
            return builder.toString();
        }

        String getStringRepresentationOfStringArray(ArrayList<String> list) {

            StringBuilder builder = new StringBuilder(list.size());
            builder.append(list.get(0));
            for (int i = 1; i < list.size(); i++)
                builder.append(" " + list.get(i));
            return builder.toString();
        }
    }

    private enum RealTileCount {
        A(9), B(2), C(2), D(4), E(12), F(2), G(3), H(2), I(9), J(1), K(1), L(4), M(2), N(6),
        O(8), P(2), Q(1), R(6), S(4), T(6), U(4), V(2), W(2), X(1), Y(2), Z(1);

        private final int count;

        RealTileCount(int count) {
            this.count = count;
        }

        private int count() {
            return count;
        }
    }
}
